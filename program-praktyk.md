# Program zdalnych praktyk na stanowisku Game Developer

## Cele:
zapoznanie studenta z praktycznymi aspektami tworzenia gier wideo w rozproszonym zespole, obejmującymi między innymi: projektowanie, programowanie, testowanie gier a także tworzenie GDD (Game Design Document).

## Program praktyki
1. Przygotowanie wstępne.
  1. Zapoznanie się z przepisami BHP dotyczącymi pracy zdalnej.
  1. Zapoznanie się ze strukturą i organizacją firmy.
1. Praca w rozproszonym zespole.
  1. Zapoznanie się z pracą z systemem kontroli wersji i narzędziami stosowanymi do pracy zdalnej.
  1. Przybliżenie metody kanban i zapoznanie się z oprogramowaniem stosowanym do wspomagania zarządzania i projektowania.
1. Tworzenie gier:
  1. zapoznanie się z oprogramowanie stosowanym w firmie,
  1. zapoznanie się z zasadami tworzenia dokumentacji gier,
  1. zapoznanie się procesem testowaniem gier i elementami Test Driven Development,
  1. zapoznanie się z procesem publikacji gier np. udostępniania gier w Google Play.
1. Konsolidacja wiedzy i umiejętności:
  1. wykonanie prostej pracy projektowej, zgodnej z wymaganiami wybranego Game Jam,
  1. fakultatywne opublikowanie wykonanego projektu na platformie [itch.io]().
1. Zakończenie praktyki:
  1. wykonanie sprawozdania z praktyki,
  1. zaliczenie praktyki,
  1. załatwienie formalności związanych z zakończeniem praktyki.

## Efekty kształcenia – umiejętności i kompetencje.
Pozyskanie praktycznej wiedzy z zakresu tworzenia gier w środowisku Unity3D, wykorzystania narzędzi kontroli wersji dla efektywnej pracy w rozproszonym zespole, nabycie więdzy i umiejętności potrzebnych przy produkcji gier.
