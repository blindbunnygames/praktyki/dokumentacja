# Umowa o studencką praktykę zawodową
zawarta w dniu. . . . . . . . . . . . r.
w Zdzieszowicach

pomiędzy:

Bartoszem Matuszewski<a id="#przedsiebiorca"></a>  
ul. Wschodnia 34  
47-330 Zdzieszowice  
NIP 1990033860  
zwanym dalej **Przedsiębiorcą**

a Panem/Panią . . . . . . . . . . . <a id="#praktykant"></a>  
legitymującym/ą się dowodem osobistym (seria,nr) . . . . . . . . .  
posiadającym nr PESEL . . . . . . . . . . . .  . .,  
zwanym/ą dalej **Praktykantem**.



## §1. Definicje
- <a id='umowa'>umowa</a> - niniejsza umowa
- <a id='praktyka'>praktyka</a> - studencka praktyka zawodowa której dotyczy [umowa][]
- <a id='strony'>strony</a> - łączenie [Przedsiębiorca][] i [Praktykant][] zorganizowana na zasadach określonych w [umowie][]
- <a id="gra">gra</a> - projekt informatyczny realizowany przez [Przedsiębiorcę][]

[Przedsiębiorca]: #przedsiebiorca
[Przedsiębiorcy]: #przedsiebiorca
[Przedsiębiorcę]: #przedsiebiorca
[Przedsiębiorcą]: #przedsiebiorca
[Praktykant]: #praktykant
[Praktykanta]: #praktykant
[Praktykantowi]: #praktykant
[Praktykantem]: #praktykant
[praktykę]: #praktyka
[praktyki]: #praktyka
[Praktyka]: #praktyka
[grę]: #gra
[gry]: #gra
[zysk]: #zysk
[zysku]: #zysk
[Zysk]: #zysk
[Strony]: #strony
[stron]: #strony
[licencja]: licencja-assetów.md
[licencji]: licencja-assetów.md
[umowa]: #umowa
[umowie]: #umowa
[umowy]: #umowa
[procent]: #procent
[Procent]: #procent


## §2. Wstępne postanowienia
1. W sprawach nieuregulowanych w umowie stosuje się powszechnie obowiązujące przepisy prawa, w tym przepisy kodeksu  cywilnego.

1. Wszelkie zmiany umowy wymagają zachowania formy pisemnej pod rygorem nieważności.

1. Każdej ze stron przysługuje prawo rozwiązania umowy w każdym czasie. Rozwiązanie umowy wymaga zachowania formy pisemnej.

1. Wszelkie spory mogące wynikać w związku z realizacją umowy zostaną poddane rozstrzygnięciu sądu powszechnego właściwego dla siedziby [Przedsiębiorcy][].

1. Umowę sporządzono w dwóch jednobrzmiących egzemplarzach, po jednym dla każdej ze [stron][].

## §3. Ogólne postanowienia
<!-- zostaną przyjęci -->
1. [Przedsiębiorca][] przyjmuje [Praktykanta][] na [praktykę][].

1. [Praktykant][] przenosi prawa autorskie na [Przedsiębiorcę][] zgodnie z treścią [umowy][].

1. [Przedsiębiorca][] umieści imię i nazwisko [Praktykanta][] w napisach końcowych gry.

1. [Przedsiębiorca][] udziela [Praktykantowi][] [licencji][] na utwory stworzone w trakcie trwania [praktyki][].

1. [Przedsiębiorca][] zobowiązuje się do wypłacenia [Praktykantowi][] procentu od zysku ze sprzedaży [gry][].

## §4. Praktyka
1. [Praktykant][] oświadcza, iż jest studentem w rozumieniu przepisów ustawy – Prawo o szkolnictwie wyższym.

1. [Praktykant][] wyraża wolę odbycia praktyki zawodowej u [Przedsiębiorcy][].

#### 4.1 Cel i organizacja praktyki
1. [Praktykant][] odbywa [praktykę][] u [Przedsiębiorcy][] w celu uzyskania doświadczenia i nabycia umiejętności praktycznych właściwych dla pracy na stanowisku game-developer, które zdobędzie uczestnicząc w realizacji [gry](#gra).  

1. [Praktyka][] odbędzie się poza siedzibą [Przedsiębiorcy][] w formie zdalnej przy użyciu środków komunikacji teleinformatycznej.

1. Poszczególne dni i godziny, w których odbywana będzie [praktyka][] ustala na bieżąco sam [Praktykant][], ewentualny harmonogram może powstać tylko za zgodą [stron][].

1. [Strony][] zgodnie oświadczają, iż umowa o studencką praktykę zawodową:

    1. nie ma charakteru umowy o pracę i nie znajdują do niej zastosowania przepisy prawa pracy,

    1. ma charakter nieodpłatny.

1. Przy odbywaniu praktyk, [Praktykant][] zobowiązany jest:
    1. wykonywać powierzone czynności z najwyższą starannością.

    1. stosować się do poleceń opiekuna praktyki lub innych upoważnionych osób,

    1. powstrzymać się od jakichkolwiek działań utrudniających pracę [Przedsiębiorcy][]

1. [Przedsiębiorca][] zobowiązuje się do:
    1. opracowania planu praktyki,

    1. wyznaczenia opiekuna, który odpowiada za przebieg praktyki, organizuje pracę praktykanta oraz sprawuje nad nim nadzór,

    1. przeszkolenia praktykanta z zakresu bezpieczeństwa i higieny pracy, przepisów przeciwpożarowych, ochrony danych osobowych oraz przeprowadzenia instruktażu stanowiskowego,

## §5. Przeniesienie praw autorskich
1. [Praktykant][] zobowiązuje się nieodpłatnie przenieść na [Przedsiębiorcę][] majątkowe prawa autorskie do utworów stworzonych w ramach odbywanej praktyki na wszystkich wyszczególnionych poniżej polach eksploatacji
wraz z wyłącznym prawem do zezwalania na wykonanie autorskich **praw zależnych** do utworów.

#### 5.1 Pola eksploatacji
1. Przeniesienie praw, o których mowa w §5 ust. 1

    1. odnosi się do projektów utworów jak ich ich postaci ukończonej,

    1. nie jest ograniczone pod względem celu ich rozpowszechniania, ani też pod względem czasowym czy terytorialnym,

    1. prawa te mogą być przenoszone przez Przedsiębiorcę na inne podmioty bez żadnych ograniczeń.

1. Przeniesienie praw, o których mowa w §5 ust. 1 obejmuje w szczególności następujące pola eksploatacji:
    1. utrwalanie lub zwielokrotnianie:

        1. trwałe lub czasowe

        1. w całości lub w części,

        1. jakimikolwiek środkami,

        1. w jakiejkolwiek formie, niezależnie od formatu, systemu
        lub standardu,

        1. w tym wprowadzanie do pamięci komputera

        1. wytwarzanie określoną techniką egzemplarzy utworu:

              1. w tym techniką drukarską,

              1. reprograficzną,

              1. zapisu magnetycznego,

              1. oraz techniką cyfrową,

        1. sporządzenia kopii zapasowej.

    1. wprowadzanie do obrotu, użyczenie lub najem oryginału albo egzemplarzy,

    1. tworzenie nowych wersji i adaptacji (tłumaczenie, przystosowanie, rozbudowę, zmianę układu lub jakiekolwiek inne zmiany i modyfikacje),  

    1. przekształcania formatu pierwotnego na dowolny inny format i dostosowywanie do platform sprzętowo-systemowych,

    1. prawo do przerobienia (dostosowania) utworu oraz łączenia jego fragmentów z innymi utworami i ich dostosowywania;
    <!-- mogę publicznie prezentować assety np. na youtube -->
    1. wszelkie rozpowszechnianie utworu w sposób inny, niż wyżej określony:

        1. publiczne wykonanie, wystawienie, wyświetlenie, odtworzenie oraz nadawanie,

        1. w tym wprowadzania zapisów utworu do pamięci komputerów i serwerów sieci komputerowych, w tym ogólnie dostępnych w rodzaju Internet i udostępniania ich użytkownikom takich sieci,

        1. przekazywania lub przesyłania zapisów utworu pomiędzy komputerami, serwerami i użytkownikami (korzystającymi), innymi odbiorcami, przy pomocy wszelkiego rodzaju środków i technik,

        1. publiczne udostępnianie utworu

            1. zarówno odpłatne, jak i nieodpłatne

            1. w tym w trakcie prezentacji i konferencji

            1. oraz w taki sposób, aby każdy mógł mieć do niego dostęp w miejscu i w czasie przez siebie wybranym, w tym także w sieciach telekomunikacyjnych i komputerowych lub w związku ze świadczeniem usług telekomunikacyjnych, w tym również - z zastosowaniem w tym celu usług interaktywnych.

1. Prawo do eksploatacji na polach wymienionych w ust. 2 odnosi się do eksploatacji utworu w części lub w całości, zarówno w postaci pierwotnej,jak i w postaci opracowania, samodzielnie lub w utworach innych podmiotów, a także w połączeniu z utworami innych podmiotów.

1. [Praktykant][] zezwala [Przedsiębiorcy][] także na:

    1. modyfikowanie utworu w tym m.in. do dokonywania korekt, skrótów, przeróbek, zmian i adaptacji utworu oraz jego pojedynczych fragmentów, łączenia utworu z innym utworem bez nadzoru autorskiego,

    1. swobodne korzystanie z utworu oraz jego pojedynczych elementów, jak również swobodne korzystanie z modyfikacji utworu i jego elementów bez nadzoru autorskiego zarówno na terytorium Polski i za granicą.

1. [Praktykant][] upoważnia [Przedsiębiorcę][] do:

    1. oznaczania utworów niezależnie od sposobu ich publikacji  przez podanie  imienia i nazwiska  [Praktykanta][], bądź do publikowania (rozpowszechniania) ich bez wskazania autorstwa, w zależności od potrzeb [Przedsiębiorcy][], jeżeli jest to podyktowane charakterem eksploatacji utworów.

    1. wykonywania w jego imieniu autorskich praw osobistych do przedmiotowych utworów, w tym prawa do decydowania o pierwszym publicznym udostępnianiu, do nadzoru autorskiego oraz do nienaruszalności formy i treści utworów oraz do ich rzetelnego wykorzystania (integralność).

## §6. Praktykant w napisach końcowych
1. [Praktykant][] wyraża zgodę aby [Przedsiębiorca][] umieścił imię i nazwisko [Praktykanta][] w napisach końcowych [gry][],

1. [Przedsiębiorca][] zobowiązuje się umieścić imię i nazwisko [Praktykanta][] w napisach końcowych [gry][].

## §7. Udzielenie licencji
1. [Przedsiębiorca][] zobowiązuje się do udzielenia [licencji][] na utwory stworzone na potrzeby gry, które powstały w trakcie trwania praktyki, a ich autorem jest [Praktykant][] jak również inne osoby powiązane z [Przedsiębiorcą][]

1. Treść [licencji][] stanowi [załącznik nr][licencja] 1 i jest integralną częścią umowy,

1. Pewne utwory mogą zostać wykluczone z licencji, o tym fakcie [Praktykant][] zostanie poinformowany przed rozpoczęciem nad nimi prac.

1. Utwory w formie cyfrowej zostaną [Praktykantowi][] udostępnione w internecie.

1. Wykaz licencjonowany utworów:

    1. będzie prowadzony w formie elektronicznej,
    2. zostanie udostępniony w internecie do wglądu,
    3. zostanie sporządzony w formie pisemnej w trakcie formalności związanych z zaliczeniem praktyki.

## §8. Udział w zysku ze sprzedaży gry
1. [Przedsiębiorca][] zobowiązuje się wypłacić [Praktykantowi][] [procent][] z zysku ze sprzedaży [gry][].

1. [Praktykant][] nie ma udziału w stratach [Przedsiębiorcy][].

1. [Procent][] zysku zostanie wyliczony proporcjonalnie do wkładu [Praktykanta][] w stworzenie [gry][]
  1. <a id='procent'></a>Procent zostanie wyliczony następującym wzorem:<br/>  
  $`procentZysku = 76.5\% {{LiczbaGodzinPraktyki} \over {1440}}`$

1. Na <a id='zysk'>zysk</a> ze sprzedaży [gry][] składa się zysk ze sprzedaży egzemplarzy, kopii i licencji:

    1. gry

    2. konwersji gry na inne platformy

    3. edycji specjalnych gry, w tym edycji rozszerzonych,    

    4. zysku z reklam wyświetlanych w grze.  

1. [Procent][] [zysku][] będzie wypłacany:
    1. tak długo jak [zysk][] za rok wcześniejszy będzie większy niż 1000zł, nie krócej niż 2 lat od dnia publikacji [gry][],

    1. comiesięcznie w pierwszych 2 latach od dnia publikacji [gry][], corocznie w pozostałych latach latach.

1. [Przedsiębiorca][] zobowiązuje się opublikować grę do dnia 1.03.2018
