Załącznik nr 1

# Licencja "Zaprzyjaźnionych" z BlindBunnyGames
Niniejsza licencja stosowana jest w odniesieniu do utworu (w rozumieniu definicji poniżej) udostępnianego na warunkach niniejszej licencji. Każde wykorzystanie utworu inne niż dozwolone na mocy niniejszej licencji jest zakazane (w zakresie, w jakim takie wykorzystanie wchodzi w zakres prawa przysługującego posiadaczowi praw autorskich do utworu).

# 1. Definicje
W niniejszej licencji stosuje się następujące definicje:

* licencja: niniejsza licencja,
* „licencjodawca”: osoba fizyczna bądź prawna, która rozpowszechnia lub udostępnia utwór na podstawie licencji,
* „licencjobiorca”: każda osoba fizyczna lub prawna, która w jakimkolwiek zakresie korzysta z utworu na warunkach licencji,
* "licencjobiorca finalny": licencjobiorca utworu finalnego,
* utwór oryginalny: utwór lub oprogramowanie rozpowszechniane lub udostępniane przez licencjodawcę na podstawie niniejszej licencji, dostępne w postaci kodu źródłowego, a także – stosownie do przypadku – kodu wykonywalnego,
* utwór inny: utwór lub oprogramowanie, którego autorem nie jest licencjodawca,
* utwory zależne: utwory lub oprogramowanie, które mogą zostać opracowane przez licencjobiorcę na podstawie utworu oryginalnego lub modyfikacji utworu oryginalnego,
* "utwór zbiorczy": utwór będący agregacją przynajmniej jednego utworu oryginalnego lub jego modyfikacji i utworów zależnych lub utworów innych,
* utwór finalny: utwór zależny w formie oprogramowania, nie będący utworem zbiorczym ani modyfikacją utworu oryginalnego, nie zawierający materiałów źródłowych,
* „utwór”: utwór oryginalny lub związane z nim utwory zależne,
* „kod źródłowy”: postać utworu czytelna dla człowieka, najdogodniejsza do analizy i modyfikacji,
* „kod wykonywalny”: każdy kod, który został zasadniczo skompilowany i który jako program przeznaczony jest do interpretacji przez komputer,
* materiał źródłowy - kod źródłowy lub postać utworu najdogodniejsza do modyfikacji np. pliki w formacie programów do edycji grafiki,
* „rozpowszechnianie” lub „udostępnianie”: każdy przypadek sprzedaży, przekazania, wypożyczenia, najmu, rozpow­szechnienia, udostępnienia, nadania lub innego oddania kopii utworu do dyspozycji innej osobie fizycznej bądź prawnej lub zapewnienia im dostępu do jego istotnych funkcji w trybie on-line lub off-line.

# 2. Zakres praw przyznawanych na mocy licencji
Licencjodawca niniejszym udziela licencjobiorcy obowiązującej na całym świecie, bezpłatnej, niewyłącznej licencji, obejmującej prawo do udzielania dalszych licencji, na wykonywanie następujących czynności przez okres obowiązywania praw autorskich do utworu oryginalnego:
* zwielokrotnianie utworu i utworów zależnych,
* modyfikacja utworu oryginalnego oraz opracowywanie utworów zależnych na podstawie utworu,
* publiczne udostępnianie utworów finalnych, w tym, stosownie do przypadku, prawo do publicznego udostępniania lub prezentacji utworu finalnego lub jego kopii oraz publicznego wykonywania utworu finalnego,
* rozpowszechnianie utworów finalnych lub ich kopii,
* wypożyczanie i najem utworów finalnych,
* sublicencjonowanie praw do utworu finalnych.

# 3. Obowiązki licencjobiorcy
Przyznanie praw, o których mowa powyżej, podlega określonym ograniczeniom i obowiązkom nałożonym na licencjo­biorcę. Obowiązki te obejmują:

## 3.1. Klauzula zakazu wprowadzania zmian
Jeżeli licencjobiorca rozpowszechnia lub udostępnia kopie utworów finalnych, takie rozpowszechnianie lub udostępnianie musi odbywać z zastrzeżeniem, że licencjobiorcy trzeci nie mogą licencjonować dalej własnych modyfikacji ani utworów zależnych stworzonych na podstawie utworów finalnych.

## 3.2. Ochrona kodu źródłowego
Rozpowszechniając lub udostępniając kopie utworu zależnego, licencjobiorca dba aby materiały źródłowe nie był dostępny.

## 3.3. Ochrona prawna
Niniejsza licencja nie upoważnia do korzystania z nazw handlowych, znaków towarowych, znaków usługowych ani nazw licencjodawcy, z wyjątkiem przypadków uzasadnionego i zwyczajowo przyjętego opisu pochodzenia utworu i powielania treści informacji o prawach autorskich.

# 4. Ciąg autorstwa
Pierwotny licencjodawca gwarantuje, że prawa autorskie do utworu oryginalnego przyznane na mocy niniejszej licencji stanowią jego własność lub że posiada je na mocy stosownej licencji oraz że jest uprawniony i upoważniony do udzielania licencji.


# 5. Wyłączenie gwarancji
Utwór ma charakter utworu nieukończonego, wciąż udoskonalanego przez wielu współautorów. Nie stanowi on utworu gotowego i dlatego może posiadać wady lub błędy właściwe dla tej formy opracowywania utworów.

Z powyższych względów utwór przekazuje się na mocy licencji w postaci, w jakiej jest udostępniany, i bez jakichkolwiek gwarancji dotyczących utworu, w tym między innymi bez gwarancji przydatności handlowej, gwarancji przydatności do określonego celu, gwarancji braku wad lub błędów, gwarancji dokładności, gwarancji braku naruszenia praw własności intelektualnej innych niż prawa autorskie, o których mowa w pkt 6 niniejszej licencji.

Niniejsze wyłączenie gwarancji stanowi istotny element licencji oraz warunek przyznania jakichkolwiek praw do utworu.

# 6. Wyłączenie odpowiedzialności
Z zastrzeżeniem przypadków działań umyślnych na szkodę osób fizycznych lub szkód wyrządzonych bezpośrednio osobom fizycznym, licencjodawca w żadnym wypadku nie ponosi odpowiedzialności za bezpośrednie lub pośrednie, majątkowe lub osobiste szkody dowolnego rodzaju powstałe w związku z licencją lub eksploatacją utworu, w tym między innymi za szkody związane z utratą wartości firmy, przerwami w pracy, awarią bądź zakłóceniami pracy komputera, utratą danych ani za wszelkie inne szkody gospodarcze, nawet jeżeli licencjodawca został poinformowany o możliwości ich wystąpienia. Licencjodawca ponosi jednak odpowiedzialność zgodnie z ustawowymi przepisami regulującymi kwestie odpowiedzialności za produkt w zakresie, w jakim takie przepisy mają zastosowanie do utworu.

# 7. Wygaśnięcie licencji
Licencja i prawa przyznane na jej mocy wygasają automatycznie z chwilą naruszenia przez licencjobiorcę warunków licencji.
